import cv2
import dlib

input_image = input ("Enter the name of image:" )

image = cv2.imread(input_image)

face_detector = dlib.get_frontal_face_detector()

img = face_detector(image)

if img:
    print("Face Detected!")
else:
   print("Please insert a valid photo!")