import flask
import os
import requests
import cv2
import dlib

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/enimg', methods=['POST'])
def home():

	#url = 'https://apod.nasa.gov/apod/image/1701/potw1636aN159_HST_2048.jpg'
	url = request.form['imgurl']
	page = requests.get(url)

	f_ext = os.path.splitext(url)[-1]
	f_name = 'img1{}'.format(f_ext)
	with open(f_name, 'wb') as f:
		f.write(page.content)
	
	image = cv2.imread('img1.jpg', 0)

	face_detector = dlib.get_frontal_face_detector()

	img = face_detector(image)

	if img:

		return ("detected")

	else:

		return ("undetected")

	return ''' <img src="img.jpg" height="100%" width="100%"> '''

app.run()